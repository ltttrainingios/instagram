//
//  MainViewModel.swift
//  training_Instagram
//
//  Created by Nguyễn  Mạnh Đức on 10/2/18.
//  Copyright © 2018 Nguyễn  Mạnh Đức. All rights reserved.
//

import Foundation

import RxSwift
import RxCocoa

enum ViewType {
    case Grid
    case List
}

class MainViewModel {
    var viewType = BehaviorRelay<ViewType>(value: ViewType.Grid)
    var photoList = BehaviorRelay<[PhotoModelElement]>(value: [])
    var visibleIndexPath: [IndexPath] = []
    
    init() {
    }
    
    func setViewType(isGrid: Bool) {
        if (isGrid ? ViewType.Grid : ViewType.List) != viewType.value {
            viewType.accept(isGrid ? ViewType.Grid : ViewType.List)
        }
    }
    
    func getPhotoListByIndex(index: Int) {
        ApiHelper.instance.getPhotoList(index: index, viewType: viewType.value) { (it) in
            index > 1 ? self.photoList.accept(self.photoList.value + it) : self.photoList.accept(it)
        }
    }
    
    func getCurrentIndexPath() -> IndexPath {
        let sortedList = self.visibleIndexPath.sorted(by: { (indexPath0, indexPath1) -> Bool in
            indexPath0.row < indexPath1.row
        })
        return (viewType.value == ViewType.Grid) ? sortedList[0] : sortedList[sortedList.count - 1]
    }
}
