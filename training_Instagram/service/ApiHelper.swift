//
//  ApiHelper.swift
//  training_Instagram
//
//  Created by Nguyễn  Mạnh Đức on 10/3/18.
//  Copyright © 2018 Nguyễn  Mạnh Đức. All rights reserved.
//

import Foundation
import Alamofire

class ApiHelper {
    final let CLIENT_KEY = "47922c0f793033d6d0da637fa3e72fc450f5e87edac0b5db0fa002b0783d6323"
    final let BASE_URL = "https://api.unsplash.com/photos/"
    let headers = [
        "Accept-Version":"v1"
    ]
    
    static let instance = ApiHelper()
    
    func getPhotoList(
        index: Int,
        viewType: ViewType,
        completionHandler: @escaping (_ point: [PhotoModelElement]) -> Void
        ) {
        let perPage = viewType == ViewType.Grid ? "30" : "30"
        let url = "\(BASE_URL)?client_id=\(CLIENT_KEY)&page=\(index)&per_page=\(perPage)"
        Alamofire.request(url, method: .get, headers: headers).responseJSON { response in
            switch(response.result) {
            case .success(_):
//                print(response.result.value ?? "")
                if let data = response.data {
                    let photoModel = try? JSONDecoder().decode(PhotoModel.self, from: data)
                    if let photoModel = photoModel {
                        //handle data here
                        completionHandler(photoModel)
                    }
                }
            case .failure(let error):
                print(error)
            }
        }
    }
}
