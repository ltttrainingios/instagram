//
//  ViewController.swift
//  training_Instagram
//
//  Created by Nguyễn  Mạnh Đức on 10/2/18.
//  Copyright © 2018 Nguyễn  Mạnh Đức. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ViewController: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView!
    
    var disposeBag = DisposeBag()
    let viewModel = MainViewModel()
    var index = 1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.register(UINib(nibName: "ImageCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ImageCollectionViewCell")
        collectionView.register(UINib(nibName: "FeedCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "FeedCollectionViewCell")
        collectionView.register(UINib(nibName: "HeaderCollectionViewCell", bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderCollectionViewCell")
        let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout // casting is required because UICollectionViewLayout doesn't offer header pin. Its feature of UICollectionViewFlowLayout
        layout?.sectionHeadersPinToVisibleBounds = true
        
        let refreshControl: UIRefreshControl = {
            let refreshControl = UIRefreshControl()
            refreshControl.addTarget(self, action:
                #selector(handleRefresh(_:)), for: UIControlEvents.valueChanged)
            refreshControl.tintColor = UIColor.black
            return refreshControl
        }()
        collectionView.addSubview(refreshControl)
        viewModel.viewType.asObservable().subscribe { (viewType) in
            self.collectionView.reloadData()
            if self.viewModel.visibleIndexPath.count > 0 {
                self.collectionView.scrollToItem(at: self.viewModel.getCurrentIndexPath(),
                    at: .bottom, animated: false)
            }
        }
        .disposed(by: disposeBag)
        viewModel.photoList.asObservable().subscribe(onNext: { (_) in
            self.collectionView.reloadData()
        })
        .disposed(by: disposeBag)
        viewModel.getPhotoListByIndex(index: 1)
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        viewModel.getPhotoListByIndex(index: 1)
        refreshControl.endRefreshing()
    }
}

extension ViewController: HeaderCellDelegate {
    func viewTypeChange(isGrid: Bool) {
        viewModel.visibleIndexPath = collectionView.indexPathsForVisibleItems
        viewModel.setViewType(isGrid: isGrid)
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    // header height
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: collectionView.frame.width, height: 180)
    }
}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.photoList.value.count
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == viewModel.photoList.value.count - 2 {
            self.index += 1
            viewModel.getPhotoListByIndex(index: index)
        }
    }
    
    // header
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            let reusableview = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "HeaderCollectionViewCell", for: indexPath) as! HeaderCollectionViewCell
            reusableview.setData()
            reusableview.delegate = self
            return reusableview
        default:  fatalError("Unexpected element kind")
        }
    }
    
    // cell
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch viewModel.viewType.value {
        case ViewType.Grid:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as! ImageCollectionViewCell
            cell.setData(photoElement: viewModel.photoList.value[indexPath.row])
            cell.layer.borderColor = UIColor.white.cgColor
            cell.layer.borderWidth = 0.5
            return cell
        case ViewType.List:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeedCollectionViewCell", for: indexPath) as! FeedCollectionViewCell
            cell.setData(photoElement: viewModel.photoList.value[indexPath.row])
            cell.layer.borderColor = UIColor.white.cgColor
            cell.layer.borderWidth = 0.5
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return viewModel.viewType.value == ViewType.Grid ? CGSize(width: screenWidth/3, height: screenWidth/3) : CGSize(width: screenWidth, height: 490)
    }
    
    //line spacing
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    //item spacing
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    // Screen width.
    public var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }
    
    // Screen height.
    public var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }
}
