//
//  HeaderCollectionViewCell.swift
//  training_Instagram
//
//  Created by Nguyễn  Mạnh Đức on 10/2/18.
//  Copyright © 2018 Nguyễn  Mạnh Đức. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

protocol HeaderCellDelegate: class {
    func viewTypeChange(isGrid: Bool)
}

class HeaderCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var gridBtn: UIButton!
    @IBOutlet weak var listBtn: UIButton!
    
    var disposeBag = DisposeBag()
    weak var delegate: HeaderCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    public func setData() {
        gridBtn.rx.tap.bind {
            self.delegate?.viewTypeChange(isGrid: true)
            }
            .disposed(by: disposeBag)
        
        listBtn.rx.tap.bind {
            self.delegate?.viewTypeChange(isGrid: false)
            }
            .disposed(by: disposeBag)
    }
}
