//
//  FeedCollectionViewCell.swift
//  training_Instagram
//
//  Created by Nguyễn  Mạnh Đức on 10/2/18.
//  Copyright © 2018 Nguyễn  Mạnh Đức. All rights reserved.
//

import UIKit
import Kingfisher

class FeedCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var userImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var feedImageView: UIImageView!
    @IBOutlet weak var loveImageView: UIButton!
    @IBOutlet weak var contentLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setData(photoElement: PhotoModelElement) {
        userNameLabel.text = photoElement.user?.username
        contentLabel.text = photoElement.user?.bio
        
        feedImageView.kf.setImage(with: URL(string: photoElement.urls?.small ?? ""))
        userImageView.kf.setImage(with: URL(string: photoElement.user?.profileImage?.small ?? ""))
        userImageView.setRounded()
    }
}
