//
//  ImageCollectionViewCell.swift
//  training_Instagram
//
//  Created by Nguyễn  Mạnh Đức on 10/2/18.
//  Copyright © 2018 Nguyễn  Mạnh Đức. All rights reserved.
//

import UIKit

class ImageCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var feedImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    func setData(photoElement: PhotoModelElement) {
        let url = URL(string: photoElement.urls?.small ?? "")
        feedImageView.kf.setImage(with: url)
    }
}
